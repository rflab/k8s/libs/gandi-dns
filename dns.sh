#!/usr/bin/env sh
set -x

curl --version

fqdn="$1"
shift 1
ip=$(curl -s ifconfig.me)

DOMAINS=$(for dom in $@; do
    echo "$dom  300 IN  A   $ip"
done)


curl --verbose -X PUT -H "Content-Type: text/plain" \
    -H"X-Api-Key: $GANDI_API_KEY" \
    --data-binary @- \
    "https://dns.api.gandi.net/api/v5/domains/$fqdn/records" \
    <<EOF
${DOMAINS}
EOF
