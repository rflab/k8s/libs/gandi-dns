{
  _config:: {
    gandi: {
      api_key: {
        name: "apikey",
        key: "token",
      },
      fqdn: "",
      subdomains: [],
      name: "dyndns",
      schedule: "0 * * * *",
      script: "",
    }
  },
  local k = import "ksonnet-util/kausal.libsonnet",
  local cronjob = k.batch.v1beta1.cronJob,
  local container = k.core.v1.container,
  local volume = k.core.v1.volume,
  local c = $._config,

  gandi: {
    cronjob: cronjob.new(
      c.gandi.name, c.gandi.schedule, containers=[
        container.new(c.gandi.name, "curlimages/curl")
        + container.withCommand(
          std.flattenArrays([
            ["sh", "/srv/dns.sh", c.gandi.fqdn]
            , c.gandi.subdomains
          ])
        )
        + container.withVolumeMounts([
          k.core.v1.volumeMount.new(c.gandi.name, "/srv/")
        ])
        + container.withEnv(
          k.core.v1.envVar.fromSecretRef("GANDI_API_KEY",
             c.gandi.api_key.name, c.gandi.api_key.key)
        )
      ])
    + cronjob.spec.jobTemplate.spec.template.spec.withVolumes([
      volume.fromConfigMap(c.gandi.name, c.gandi.name)
    ])
    + cronjob.spec.jobTemplate.spec.template.spec.withRestartPolicy("Never"),

    local configMap = k.core.v1.configMap,
    configMap: configMap.new(c.gandi.name, {
      "dns.sh": importstr 'dns.sh'
    }),
  }
}
